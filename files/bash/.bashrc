##
##   _                         _             _
##  | |__  _ __ __ _ _ __   __| | ___  _ __ (_) __ _
##  | '_ \| '__/ _` | '_ \ / _` |/ _ \| '_ \| |/ _` |
##  | |_) | | | (_| | | | | (_| | (_) | | | | | (_| |
##  |_.__/|_|  \__,_|_| |_|\__,_|\___/|_| |_|_|\__,_|
##
##
##  ~/.bashrc
##
##

export PS1="[\[\033[00;31m\]\u\[$(tput sgr0)\]@\[\033[00;35m\]\h \[\033[36m\]\w\[$(tput sgr0)\]]\$ "
export RPS1="<[%!]>"
export BROWSER=/usr/bin/firefox

PATH="$PATH:/home/brandonia/.local/bin:/home/brandonia/infra_rez/bin"; export PATH

# Alias
##-----
alias susp='systemctl suspend'
alias tmux='tmux -2'
alias ls='ls  --color=auto'
alias grep='grep --color'
alias la='ls -al'
alias ll='ls -ll'
# alias mcb='multiCB.py'
# alias anime-tracker='anime-tracker.py --file /mnt/nas/Projects/anime-tracker.xlsx'
alias tree='tree -C'
alias wtr='curl wttr.in'
# alias st='curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3 -'
alias vim='nvim'


# git
##---
alias gl='git log --graph --pretty=format:"%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit'
alias gs='git status'

# color scripts
##-------------
# colorscript -r
pokemon-colorscripts -r --no-title

# start sway on tty login
##-----------------------
if [ -z "${WAYLAND_DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec Hyprland
fi
