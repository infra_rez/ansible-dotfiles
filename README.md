ansible-dotfiles
---

# Objective

An ansible role to set up various configuration files.

- bash
- foot
- alacritty
- sxhkd
- tmux

# Parameters

Variables are set on some things so they can be tweaked. In an effort to keep things modular, `True` or `False` can be set. Check the vars directory in the role.

```
bash: true
foot: true
alacritty: false
qtile: false
tmux: true
```